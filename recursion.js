function reduce(arr, fn, initial) {
  var recurseReduce = function(index, value) {
    if (index > arr.length - 1) return value
    return recurseReduce(index + 1, fn(value, arr[index], index, arr))
  }

  return recurseReduce(0, initial)
}

module.exports = reduce
