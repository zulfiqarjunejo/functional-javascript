function repeat(operation, num) {
  // modify this so it can be interrupted
  if (num <= 0) return
  setTimeout(operation, 10);
  return repeat(operation, --num)
}

module.exports = repeat
