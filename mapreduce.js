module.exports = function arrayMap(arr, fn) {
  return arr.reduce(function(prev, item) {
    prev.push(fn(item));
    return prev;
  }, [])
}
