function Spy(target, method) {
  let spy = {
    count : 0
  };

  let oldFunction = target[method];
  target[method] = function() {
    spy.count++;
    return oldFunction.apply(target, arguments);
  }
  return spy;
}

module.exports = Spy
