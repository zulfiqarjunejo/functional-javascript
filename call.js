function duckCount() {
  return Array.from(arguments).filter((duck) => {
    return Object.prototype.hasOwnProperty.call(duck, 'quack')
  }).length;
}

module.exports = duckCount
